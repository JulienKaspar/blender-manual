.. index:: Geometry Nodes; Matrix Determinant
.. _bpy.types.FunctionNodeMatrixDeterminant:

***********************
Matrix Determinant Node
***********************

.. figure:: /images/node-types_FunctionNodeMatrixDeterminant.webp
   :align: right
   :alt: Matrix Determinant node.

The *Matrix Determinant* node computes the `determinant <https://en.wikipedia.org/wiki/Determinant>`__ of the passed
in matrix.


Inputs
======

Matrix
   The matrix to compute the determinant of.


Properties
==========

This node has no properties.


Outputs
=======

Determinant
   The compute determinant value.
