.. index:: Geometry Nodes; Transform Direction
.. _bpy.types.FunctionNodeTransformDirection:

************************
Transform Direction Node
************************

.. figure:: /images/node-types_FunctionNodeTransformDirection.webp
   :align: right
   :alt: Transform Direction node.

The *Transform Direction* node multiplies a :term:`Transformation Matrix` by a vector.


Inputs
======

Direction
   The vector.
Transformation
   The transformation matrix.


Properties
==========

This node has no properties.


Outputs
=======

Direction
   The vector.
