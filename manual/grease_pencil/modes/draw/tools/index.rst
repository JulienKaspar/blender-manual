.. _bpy.types.BrushGpencilSettings:

#########
  Tools
#########

.. toctree::
   :maxdepth: 2

   brush.rst
   erase.rst
   fill.rst
   trim.rst
   eyedropper.rst
   line.rst
   polyline.rst
   arc.rst
   curve.rst
   box.rst
   circle.rst
   interpolate.rst
