.. _gpencil_sculpt-toolbar-index:

***************
Sculpting Tools
***************

Brush
   Tool to use for any of the sculpting :doc:`brushes <brushes>`.

:ref:`Annotate <tool-annotate-freehand>`
   Draw free-hand annotation.

   :ref:`Annotate Line <tool-annotate-line>`
      Draw straight line annotation.
   :ref:`Annotate Polygon <tool-annotate-polygon>`
      Draw a polygon annotation.
   :ref:`Annotate Eraser <tool-annotate-eraser>`
      Erase previous drawn annotations.
