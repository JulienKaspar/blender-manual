
******************
Vertex Paint Tools
******************

.. figure:: /images/grease-pencil_modes_vertex-paint_tools_brushes.png
   :align: right

Brush
   Tool to use for any of the vertex paint :doc:`brushes <brushes>`.

:ref:`Annotate <tool-annotate-freehand>`
   Draw free-hand annotation.

   :ref:`Annotate Line <tool-annotate-line>`
      Draw straight line annotation.
   :ref:`Annotate Polygon <tool-annotate-polygon>`
      Draw a polygon annotation.
   :ref:`Annotate Eraser <tool-annotate-eraser>`
      Erase previous drawn annotations.
