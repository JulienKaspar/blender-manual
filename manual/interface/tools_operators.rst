
#####################
  Tools & Operators
#####################

.. toctree::
   :maxdepth: 1

   tool_system.rst
   operators.rst
   undo_redo.rst
   annotate_tool.rst
   selecting.rst
