
************
Introduction
************

The Preview mode shows how the final edited video will look like. It also offers tools for
moving, rotating, and scaling images, as well as scopes for analyzing color distribution.

.. figure:: /images/editors_vse_type.svg
   :alt: Preview mode

   Preview mode of the Video Sequencer.

You can pan around the view with :kbd:`MMB` and zoom with :kbd:`Wheel` or :kbd:`NumpadPlus`/
:kbd:`NumpadMinus`. Alternatively, you can use the gizmos.

Pressing :kbd:`Home` resets the view, maximizing the size of the preview within the editor's area.
