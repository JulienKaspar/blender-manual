
################
  Output Nodes
################

These nodes are used to output the composited result in some way.

.. toctree::
   :maxdepth: 1

   composite.rst
   viewer.rst

----------

.. toctree::
   :maxdepth: 1

   file_output.rst
